module Jekyll
  module RegexFilter
    def custom_image(input, size)
      path = input.split("/");
      path[-1] = size.to_s + "_" + path[-1];
      path.join("/");
    end
  end
end

Liquid::Template.register_filter(Jekyll::RegexFilter)
