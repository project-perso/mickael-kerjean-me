#!/bin/bash

cd ./assets/thumbnails/

list=$(ls *)
for img in $list; do
    if [[ $img != 45_* && $img != 90_* && $img != 130_* && $img != 260_* ]]; then
        echo "- process: "$img
        name=$(convert $img -format "%t" info:)
        suffix=$(convert $img -format "%e" info:)
        convert $img -resize 45x45 -quality 75 "45_${name}.${suffix}"
        convert $img -resize 90x90 -quality 75 "90_${name}.${suffix}"
        convert $img -resize 130x130 -quality 75 "130_${name}.${suffix}"
        convert $img -resize 260x260 -quality 75 "260_${name}.${suffix}"
    fi
done
