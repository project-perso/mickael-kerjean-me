---
layout: post
category: pro
published: false
---

From throwing errors in your code to the final display

<!--more-->

Managing errors properly is a important part we shouldn't neglect. When I first went on this topic in my phoenix app, 
it tooks a bit more than 5 minutes to understand it and I would have like to find those notes somewhere.

# The lousy and non lousy functions
By convention, a function ending with a bang is lousy and throw error whereas the ones
without bang are more silent. Pretty much all the functions in Ecto have those 2 functions, the lousy one and the relax one. 

Fire up a REPL and see by yourself: ```iex -S mix```
```
iex(6)> MyApp.Repo. # press tab to discover the function you have access ;)
aggregate/3            aggregate/4            all/1
all/2                  config/0               delete!/1
delete!/2              delete/1               delete/2
delete_all/1           delete_all/2           get!/2
get!/3                 get/2                  get/3
get_by!/2              get_by!/3              get_by/2
get_by/3               in_transaction?/0      insert!/1
insert!/2              insert/1               insert/2
insert_all/2           insert_all/3           insert_or_update!/1
insert_or_update!/2    insert_or_update/1     insert_or_update/2
one!/1                 one!/2                 one/1
one/2                  preload/2              preload/3
query!/1               query!/2               query!/3
query/1                query/2                query/3
rollback/1             start_link/0           start_link/1
stop/1                 stop/2                 transaction/1
transaction/2          update!/1              update!/2
update/1               update/2               update_all/2
update_all/3

```

Example:
```
# query on the database to get an existing member:
MyApp.Repo.get(MyApp.Member, 1)
=> %MyApp.Member{........}
MyApp.Repo.get!(MyApp.Member, 1)
=> %MyApp.Member{........}
```
We get the same thing until our query get us no results at all:
```
MyApp.Repo.get(MyApp.Member, 1000000)
=> nil
MyApp.Repo.get!(MyApp.Member, 1000000)
=> ** (Ecto.NoResultsError) expected at least one result but got none in query:
```
The functions ending with a ! throw an error at us. 


# Elxiri try/catch/rescue

To get started with exceptions handling in elixir, the best is to look at [the elixir guides here](http://elixir-lang.org/getting-started/try-catch-and-rescue.html)

Very briefly:
```
raise "a message here"
# it's the same as:
raise RuntimeError, message: "a message here"
```
And you can create your own errors:
```
defmodule UnauthorizedError do                                                                                        |  def update(artist_params, alias) do
  defexception message: "Authentication required"                                                                     |      Artist.find(alias)
end  

raise UnauthorizedError
```

Manging your error manually would look like this:
```
try do
    # do something that fail here
rescue
    RuntimeError -> # rescue a runtime error
    UnauthorizedError -> # rescue from a custom error
end
```
But we won't directly use it in this post. It's just good to know


# Define your errors

That's what I have in my phoenix app in /lib/error.ex:
```
defmodule UnauthorizedError do
  defexception message: "Authentication required"
end

defmodule ForbiddenError do
  defexception message: "Access forbidden"
end

defmodule InternalError do
  defexception message: "Internal Error"
end

defmodule NotFoundError do
  defexception message: "Not found"
end


defimpl Plug.Exception, for: UnauthorizedError do
  def status(_), do: 401
end

defimpl Plug.Exception, for: ForbiddenError do
  def status(_), do: 403
end

defimpl Plug.Exception, for: InternalError do
  def status(_), do: 500
end

defimpl Plug.Exception, for: NotFoundError do
  def status(_), do: 404
end
```

Here, if I raise a UnauthorizedError from the code, it will send a 401 status code to the client, pretty handy!
Now we can imagine that we have a 401 page that will display a message and a link to the login page


# Templates
It tooks me some time to properly render the page and that's why I created this post. 
The way Phoenix is getting the proper file in the proper folder felt magic to me and the phoenix magic power didn't help in this case

Fire up a REPL:
```
Phoenix.View.render(MyApp.PageView, "index.html", %{conn: MyApp.Endpoint})
=>  {:safe, [["" | " .... "] | "....."]}
```