---
layout: post
category: pro
image: /assets/thumbnails/ssl.png
---

Install once and don't ever think about updating your ssl certificate. A step by step guide

<!--more-->

# What is it?
A few month ago, I discovered letsencrypt, a service that provide free SSL certificate for everyone that can be use for free and get you some piece of mind.
You'll never have to manually renew your SSL certificates.

# Process to create a certificate
To install the tool that will generate the certificate:
```
sudo apt-get install letsencrypt
```

```
DOMAIN=example.com
CERT_PATH=/var/www/letsencrypt
sudo mkdir -p $CERT_PATH
sudo letsencrypt certonly -a webroot -w $CERT_PATH -d $DOMAIN
```

# nginx configuration
- Edit nginx configuration:
``` emacs /etc/nginx/sites-enabled/default ```

- A working configuration:
```
server {
    listen 80;
    server_name example.com;
    return 301 https://$host$request_uri;
}
server {
    listen 443 ssl;
    server_name example.com;
    root /var/www/html/;

    ssl_certificate  /var/www/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key  /var/www/letsencrypt/live/example.com/privkey.pem;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

    index index.html index.htm index.nginx-debian.html;
    location / {
	try_files $uri $uri/ =404;
    }

    location ~ /.well-known {
	allow all;
    }
}
```

- Test and restart the configuration:
```
nginx -t
service nginx restart
```

You're done!
