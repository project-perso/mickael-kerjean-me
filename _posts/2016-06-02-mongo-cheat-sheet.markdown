---
layout: post
category: pro
image: /assets/thumbnails/mongo.jpg
---

Basic cheat sheet for mongo

<!--more-->

# Export data
{% highlight bash %}
DATABASE=foo
COLLECTION=foo
OUT=foo.json
mongoexport --db $DATABASE --collection $COLLECTION --out $OUT
{% endhighlight %}

# Import Data
{% highlight bash %}
INPUT=foo.json
mongoimport --db places --collection places --drop --file $INPUT
{% endhighlight %}

# Basic commands
```
# list available datababases
show dbs;
# use a database
use db_name;
# simple query
db.collection_name.find().limit(1).pretty()
# create index
db.collection_name.createIndex({field:1})
```
