---
layout: post
category: pro
image: /assets/thumbnails/password.png
---

An application to store your password securely using Keepass

<!--more-->

# Why should I care?

How many people asked me to reset their password this week? 3

My company even as a dedicate number you need to press while calling support just to manage lost passwords ....

To simple problems, simple solution, make use of a password manager.

Keepass is one tools that let you control your entire private information from one single password. No need to remind 100s, just remind 1

As they have client for every single platform, it's not an exuse not to use one!

# Let's install one

[this open source client](https://app.keeweb.info/) can be deploy in your own server. Because it doesn't persist data in memeory, it seems pretty good to me. You can even store you database in different place, and the tool will find the database by itself ;)

Fork [this repo](https://gitlab.com/project-perso/key-mickael-kerjean-me/) on gitlab and start the build, wait a few seconds, and you should have your own instance on gitlab pages. Mine can be found [here](https://project-perso.gitlab.io/key-mickael-kerjean-me/)
