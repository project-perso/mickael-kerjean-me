---
layout: post
---

A simple solution to monitor your services

<!-- more -->

# The problem
You may have many projects, they sometimes goes down for unexpected reasons, keeping track of it can be quite an annoying challenge and having system in places to get notified when something goes down is great.

There's many solutions that let you do that but they usually rely on their own set of stuff. After time trying things out I was never really satisfied with those solution and start thinking about something that would leverage existing and prooven technologies to get a simple dashboard you can look at to get an overview of the health of your things.

# A solution
A solution I have in place since a few month is based on Jenkins. Yes you probably already know Jenkins as a continuous integration tool but it is flexible enough to do way more!

Jenkins has become the landing page for pretty much everything I own, reporting everything about healtcheck and performance.
