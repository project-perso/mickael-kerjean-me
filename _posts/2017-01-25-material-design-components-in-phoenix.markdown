---
layout: post
category: pro
published: false
image: http://www.materialdoc.com/content/images/2015/11/materialdesign_introduction.png
---

Add material design component to your phoenix application

<!--more-->

There's a huge load of material design implementations with pretty much a library for every single framework wether your frameowrk of choice is react,
angular, bootstrap, ...

It shouldn't be like that, we shouldn't have to go with a frontend framework to get some material design components (vue.js, angular, react, polymer, ...) neither we
shouldn't have to ship a very tick code to the client if we're not using all of the components (getmdl, bootstrap related libs).

We should be able to say, I need the textfield component and that's it, then ship the minimum required code to make it works.
Well that's not the decision most material design implementations did with only a few exceptions like the material web component library.

We'll look at this library and integrate it in our Phoenix application.


# Introducting material web component
This library is built by people working at Google and the code is available [here](https://github.com/material-components/material-components-web)

We shouldn't considered it as a feature but considering the current landscape it's definively one, it's framework agnostic.

An example to get the textfield component working:
```
# pull the library from the npm registry
npm install --save @material/textfield

# once you got it, you can directly import the css and js files available here:
node_modules/@material/textfield/dist/mdc.textfield.min.css
node_modules/@material/textfield/dist/mdc.textfield.min.js
# or pick the non minified version in the dist folder if you prefer
```

All the available components are available as [packages](https://github.com/material-components/material-components-web/tree/master/packages),
each have a dedicated documentation ([textfield](https://github.com/material-components/material-components-web/tree/master/packages/mdc-textfield), [button](https://github.com/material-components/material-components-web/tree/master/packages/mdc-button), ...)

# Integration in our brunch asset pipeline
Phoenix has a default asset pipeline that uses Brunch.

## CSS
For the sake of development, I'm always using scss, and [here is a great and short guide](https://jimmy-beaudoin.com/posts/elixir/using-sass-import-with-elixir-phoenix-and-brunch/)
on how to configure brunch to compile you're sass files.

Once you did it, go straight to you app.scss and add this line:
```
@import '@material/textfield/mdc-textfield';
```
If you compile your assets:
```
node node_modules/brunch/bin/brunch build
```
I had this error:
```
26 Jan 05:41:04 - info: compiling
26 Jan 05:41:04 - error: Compiling of web/static/css/app.scss failed. File to import not found or unreadable: @material/textfield/mdc-textfield
Parent style sheet: /xxxxxxxx/xxxxxx/web/static/css/app.scss
26 Jan 05:41:04 - info: compiled 23 files into 2 files, copied 20 in 4.4 sec
```
We need to explicitly tell the sass compiler to include the node_modules folder in its loading path so that it knows where to look for those absolute paths.
To do that, add this to the plugins section of your brunch-config.js file:
```
 ...
 plugins: {
        // add this
        sass: {
            options: {
                includePaths: ['node_modules/']
            }
        },
        // ...
    },
    ...

```
It should now compile without errors:
```
$ node node_modules/brunch/bin/brunch build
26 Jan 05:45:16 - info: compiling
26 Jan 05:45:16 - info: compiled 24 files into 2 files, copied 20 in 4.4 sec
```

For the text field we want to respect the material design spec, we can add this class: mdc-textfield__input
That's it for the css

## JS
The libray also ship with js we need. How to instantiate our component is usually written in the component documentation.