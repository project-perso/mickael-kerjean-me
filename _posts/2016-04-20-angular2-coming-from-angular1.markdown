---
layout: post
category: pro
image: /assets/thumbnails/tux.jpg
---

This post will simply give some lessons I learned along the way by working with Angular2.

<!--more-->

# Initial Motivation for a front end framework

To me the major reason for choosing a frontend framework is to improve the User experience and solves several flaws:
- Waiting for every page to load is distracting and frustrating for the user, front end framework let you keep the control.
- Browsers are quite a bit more powerfull than PDF Viewers and JS engine are fast enough to process things we use to do on the backend and build great experience
- Sending data over the wire is much lighter than generating and sending a whole HTML page

Because nothing is free there is also drawbacks using those techs:
- new technologies that didn't exist a few years back
- browser dinosaurs are generally not supported
- initial rendering time
- SEO

# Choice of framework

Reinventing the wheel isn't something that ever went on my minds. There is already many front end framework out there and since a year, react is the big thing. But because I don't wish to eat anything from facebook, I ran away.

Having a background on Angular1, we went with Angular2 because:
- the project is backed by google, actively developed by bright people,
- Angular2 has a good [advertising](https://angular.io/features.html).
- SEO matter for the project I wanted to use it
- create an app from an existing code could become a good thing when we'll need it.

However as we'll see it, the nice list of features isn't fully here yet:
- LEGACY BROWSER SUPPORT: I had issue with an iphone4 stuck with IOS7. it doesn't event boot up.
- INTERNATIONALIZATION: well, as the official repo says, it's in alpha stage and work in progress
- CROSS PLATFORM:
- Application development: you currently have 2 choices, [ionic](http://ionicframework.com/docs/v2/) (which consist of a set of components made with CSS and HTML that is embedded into a webview with Cordova) or [Nativescript](https://www.nativescript.org/) (that make use of native devices capabilities, no HTML/CSS here). As a user of ionic v1, the V2 was missing too many feature from the previous release and I ran away. I also tried nativescript for a day and run away as well as the build process was way too long on a windows machine to do anything serious.
- [Server side rendering](https://github.com/angular/universal): I can't find it anymore but a few month back, I found a message from one of the developers of the project stating this was the kind of state of the art project and that we shouldn't expect a lot of it yet

# First impression

- [The documention](https://angular.io/docs/ts/latest/): is great, eat it all starting by the [5 minutes guide](https://angular.io/docs/ts/latest/quickstart.html) (start a timer, 5 minutes is a real challenge. It tooks me already that time just to install the depedencies from a starbuck)
- Typescript: At first glance, I wasn't super happy to use a Microsoft thing, especially since functions in js are super powerfull. However, I realized a few month back that making use of those features (apply, call, bind, clojure, prototype) can be confusing for some other developpers and I like to write code that can be read, understood and update by other. At the end, typescript achieve this goal and make the code simpler to read, easier to maintain, enabling many cool features on your editors like real autocompletion, errors highlighting (how not to like it?).
- The experience: It felt like Angular2 is simply about breaking things into little pieces that we used to call directive in Angular1, nothing new here. You're simply pushed to think such as everything is a component/directive to end up playing lego and mount your app from those little pieces. Actually Angular1 seems more complex with less freedom than this release as you're not tight to the internal mechanics of this second release (digest, dirty checks, ... and all the nasty features that are direct consequences of it: $timeout, $watch, ....)
- The parts: by reading the guides and docs, it felt like all what Angular1 has, Angular2 also has it but with a different name:
```
|-----------------------------|
| Angular 1    | Angular 2    |
|-----------------------------|
| service      | service      |
| factory      | service      |
| directive    | component    |
| directive    | directive    |
| filter       | pipe         |
| promise      | observable   |
```
A note on services in Angular2 and their counterpart in Angular1:
I never really understood why somebody would need factory AND services as what a service can do, a factory can do (and not the other way around).

# Where I struggled the most: Observable

Observable was to me the trickiest part to use Angular2. It isn't absolutely necessary to use it for trivial things but it makes your code cleaner at the end by decoupling all the part of your application.

If you wonder what an observable is, it is simply a way to use the [observer design pattern](https://en.wikipedia.org/wiki/Observer_pattern). Learning how to use it however wasn't that easy especially since a lot of people simply say:
```
observable = promise that can emit multiple times
```
No, no no and no! It doesn't make sense for two reasons:
- the promise API has a notify callback which can be use to emit multiple event if you feel like it.
- observable are way more than that!!!! Think of it as promise ++.

What I would have like to read at first to get an understanding of Observable:

- an observable should be pictured as a stream (aka a pipe of thing you can manipulate and transform easily)
- Operation available on a stream are typically:
  - creation (aka create an observable)
  - manipulate stream(s) using operators
    - To understand how you can manipulate streams, there is nothing better than a diagram to understand those operations (aka operators). [This link](http://reactivex.io/documentation/operators.html) is probably the best resources to grasp a visual picture of what all those operators are doing. For example, the [delay operator](http://reactivex.io/documentation/operators/delay.html)
    - the very large amount of operators are what makes observable truly nice. The amount of them can be quite impressing but always refer back to [here](http://reactivex.io/documentation/operators.html)

I would have love to read thoses articles first when I put my head on observable:
- [The introduction to Reactive Programming you've been missing](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
- [Your best friend to find how to process a stream](http://reactivex.io/documentation/operators.html)
