---
layout: post
category: pro
image: /assets/thumbnails/rxjs.jpg
---

Introducing subscribe, from, of, flatMap, map, filter, mergeAll in a few short example

<!--more-->

```
console.clear();
////////////////////////////////////
// Apply Transformation
// goal: emit elements of the colletion bigger than 2

var collection = [1,2,3,4,5], obs, INPUT;

////////////////////////////////////
// 1) constraint:
INPUT = collection;

/* proposed solution
obs = Rx.Observable.from(INPUT)
.filter(e => e>2)
obs.subscribe(a => console.log(a))
//*/

/////////////////////////////////////
// 2) constraint:
INPUT = Rx.Observable.of(collection)

//* baby step:
obs = INPUT.map(x => Rx.Observable.from(x))
obs.subscribe(a => a.subscribe(x => console.log(x))) // accessing a value require to nested the subscribe :(
//*/

/* solution 1:
obs = INPUT.map(x => Rx.Observable.from(x))
.mergeAll() // to avoid the observable of observable
.filter(x => x > 2)
obs.subscribe(a => console.log(a))
//*/

/* solution 2: using flatMap
obs = INPUT.flatMap(x => Rx.Observable.from(x))
.filter(x => x > 2)
obs.subscribe(a => console.log(a))
//*/
```
