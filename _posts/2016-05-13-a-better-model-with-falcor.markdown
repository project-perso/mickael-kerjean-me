---
layout: post
category: pro
image: /assets/thumbnails/netflix.jpg
---

The idea behing an async model

<!--more-->
There is a few new cool stuff in the js world everyday, relay and falcor are one of them.
The problem this libraries are solving is making your app feeling faster with a smarter network/cache strategy.
When we deal with RESTFUL apis, it’s very common to do HTTP call to an endpoint to generate a page and until this response resolve, you might display a loading indicator.

This approach feels very sub optimal as you might already have a partial view of the data that you could reuse to at least display something and makes a better experience by only loading the required amount of data in one HTTP request.

Would be great isn’t it?

Well welcome to ~~21st century~~ 2016 and let’s introduce Falcor and Relay. Both libraries try to optimize the data that come in the wire and try to solve the latency issue.
Because one of this tech is made by Facebook, I threw Relay in the box of interesting techs I’ll not put my hands on and only look at Falcor which is develop and maintain by Netflix.

Frankly, after playing with it, I felt bad for having spent such a large amount of time trying to manage HTTP call myself and creating caching strategy by myself.

The idea of falcor is simply to access the model in a async way! Why the async model solve the issue? Because you don’t assume anything regarding how the data should be fetch whether it’s locally or from the network and obviously nobody want to fetch data from a network in a sync way as it would break your UI. That the main feature of Falcor, but it also come with a bunch of interesting features:
- select only certain piece of data that you are interested in.
- 1 query to replace many - support promise and rxjs (nice!!!!!)
- a LRU, to cache data in an efficient way. It kind a make sense as Netflix should be available on the crapiest device you could find, instead of loading data and caching everything, you can choose to drop the data that is the less recently used

The approach comes with a drawback, From a developer perspective, to make sense in the server, you’d need to configure a middleware to wrap your existing apis and make them talk FQL (aka falcor query language). Note that FQL is not the official term use by netflix, but I just like it.

Another drawback is Netflix engineering guys says it’s only optimize for frequent read and not too frequent write. To start with Falcor, I’d recommend looking at this [video], then keep going with the [guide], and finish with the [documentation] that will give you all the necessary information to implement the fql server.

[video]: https://www.youtube.com/watch?v=aMAs7a_iWVQ
[guide]: http://netflix.github.io/falcor/
[documentation]: http://netflix.github.io/falcor/documentation/router.html
