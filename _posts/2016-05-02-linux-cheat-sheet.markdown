---
layout: post
category: pro
image: /assets/thumbnails/tux.jpg
---

A list of usefull commands

<!--more-->

# User management
```
# create a user
USERNAME=foo
useradd $USERNAME

# reset password
USERNAME=foo
passwd $USERNAME

# add an existing user to a group
USERNAME=foo
GROUP=www
usermod -a -G $GROUP $USERNAME
cat /user/group

# remove user
USERNAME=foo
userdel $USERNAME

# add a user to a group
USERNAME=foo
GROUP=www-data
usermod -a -G $GROUP $USERNAME
```

# Archive
```
# Create an archive
OUTPUT=archive.tar.gz
INPUT=folder
tar -zcvf $OUTPUT $INPUT

# Extract archive
OUTPUT=folder
INPUT=archive.tar.gz
tar -zxvf $INPUT -C $OUTPUT
```

# Utilities
```
# disk usage
df -h
# find out who is taking too much place
du -hsx /* | sort -rh
```

# network
```
IP_TO_CHECK=0.0.0.0
nmap -PN $IP_TO_CHECK
```
