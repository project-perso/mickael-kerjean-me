---
layout: post
title: Release of Integrapptor v1.0.0
image: /assets/thumbnails/integrapptor.png
category: pro
---

A desktop app to integrate all your web apps together

<!--more-->

Today, I'm releasing [Integrapptor](https://github.com/mickael-kerjean/integrapptor), a desktop app integrating all your web apps in one place. This will address the following issues:
-   Speeding up new employee onboarding by grouping everything they need in one interface
-   Having too many tabs open and never knowing which tab is the one I'm looking for

Integrapptor is available under the AGPL License and is built with [Electron](https://github.com/electron/electron). Therefore, it will be the same as if you were browsing your apps on Chrome.

Features:
-   Easily create your own desktop app
-   Quickly access your apps with shortcuts
-   Http Basic auth

In the roadmap:
-   Allow organisations to publish their apps for different departments and update the desktop app accordingly
-   More shortcuts
