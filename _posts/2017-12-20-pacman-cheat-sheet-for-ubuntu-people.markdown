---
layout: post
title: Pacman cheat sheet for apt-get people
image: /assets/thumbnails/tux.jpg
category: pro
---

Side to side syntax of pacman compared to ubuntu

<!--more-->

All the commands to manage your packages on Arch:

|            | Ubuntu                     | Arch        |
| install    | apt-get install            | pacman -S   |
| search     | apt-cache search           | pacman -Ss  |
| update     | apt-get update             | pacman -Syy |
| upgrade    | apt-get upgrade            | pacman -Syu |
| remove     | apt-get remove             | pacman -Rsn |
| autoremove | apt-get autoremove         | pacman -R   |
| list       | apt list &#x2013;installed | pacman -Qe  |
