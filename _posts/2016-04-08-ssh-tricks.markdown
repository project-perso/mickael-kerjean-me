---
layout: post
category: pro
image: /assets/thumbnails/ssh.jpg
---

generate keys, create and configure a tunnel

<!--more-->

# Login without password

From you local machine:
```
# variables
REMOTE_USERNAME=foo
REMOTE_HOST=example.foo
# script
ssh-keygen -t rsa
ssh-copy-id $REMOTE_USERNAME@$REMOTE_HOST
```

The ssh-copy-id does nothing more than:
```scp ~/.ssh/id_rsa.pub $REMOTE_USERNAME@$REMOTE_HOST:~/ssh/authorized_keys```


# How to watch the American version of Netflix (a kinda VPN)?

## STEP1: Establish a SSH tunnel

### On linux
```ssh -L 7071:localhost:80 $USERNAME@$HOST```

### windows
With [Putty](http://www.putty.org/), setup your hostname, then go to:
Connection => SSH => Tunnels
```
Source port: 1234
Click on Dynamic
```

## STEP2: Add the new proxy setting</h2>
From the browser: Settings => Advanced => Change proxy settings
On window, it can be found here:
```
Internet Properties => Connections => LAN Settings
check use a proxy server for your LAN => advanced => Socks: 127.0.0.1 port 1234
Then click the add button
```
