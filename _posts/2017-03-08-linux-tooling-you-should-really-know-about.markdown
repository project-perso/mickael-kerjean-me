---
layout: post
category: pro
image: /assets/thumbnails/tux.jpg
---

4 great cli tools you should know about: tmux, tig, htop, emacs

<!--more-->

Taking time to master the command line is a great investment as the linux ecosystem is absolutly amazing in this regard.

In this serie of post we'll look at a few of those tools that aren't ship by default on everyone's machine but in my opinion should.
- [tmux](#tmux): the terminal multiplexer
- [tig](#tig): a git client similar to sourcetree but cli based
- [htop](#htop): a process manager
- [emacs](#emacs): a text editor

So let's get started!

# Tmux
I still can't believe I blindly ignored tmux until 2 years ago when I decided to put a few minutes aside and look into it. This tool as literally change how I interact with a shell on a daily basis.

As most of the time in the linux world, the community hasn't done a great job in marketing and it's likely you will run away if you're not already familiar with any terminal multiplexer.

## How does it help?
Problems Tmux has solve for me:
- Working in a shell, you don't really feel the multitask capabilities of your operating system. Of course you can put some task in the foreground but there's a lot of context switch and it feels hacky.
- Working on a remote machine, if you internet connection is interrupt, you lose everything that was attach to your session, ouch
- Get multiple people seeing the same thing on a remote machine to train someone on a server for instance

## Installation
[The official website](https://tmux.github.io) of tmux. You should be able to install tmux from your prefered package manager.

## Getting started
Right after installation, tmux feels like opening emacs for the time. It's ugly and the list of shortcut you need to go through isn't intuitive.
To improve your first contact with tmux, run this command to import the default configuration of tmux I'm using:
```
curl http://mickael.kerjean.me/assets/files/tmux.conf > ~/.tmux.conf
```

Now that we're ready, let's start a new session
```
tmux
```
If you want to quit, then:
```
exit
```

### Important shortcuts

Everytime you call the hotkey (by default C-b), you can invoke tmux function like:
- C-b c: **C**reate a new tab
- C-b n: navigate to the **P**ext tab
- C-b p: navigate to the **P**revious tab
- C-b |: split the screen vertically
- C-b -: split the screen horizontally.
- C-b ,: rename the current tab
- C-b s: select another session to go into (press q to quit and go back to your session)

Other important command:
- ``` C-b d ```: detach your running session and go back to your shell
- ``` tmux ls ```: see the running sessions you can attach to
- ``` tmux attach -t $NAME ```: attach to an existing session named $NAME


# Tig

## What is it?
As you may have notice, tig is git spells backward.
If you ever install any gui based git client, tig is the cli equivalent of sourcetree and my goto tool when I want to see or do something in a git repo.

## Getting started
Let's boot it up
```
> tig
```
You should now have a view on your repo. You can do pretty much everything you would do with the git command.
I don't want to dig too much in all the shortcut here but to get started, navigation is like you would do with vi, hjkl to move in the direction you want.

The only shortcut you need to remember is h to display the help and see the power of tig.


# Emacs

In the text editor competition, Sublime and Atom are much more approachable but if a text editor would be music, Atom would be Justin Bieber whereas Emacs would be Stravinski.
Like music from Igor Stravinski, I don't know anybody who start to appreciate it the first time.

Powerfull beast can be complex to get start with, emacs is no exception and the learning curve is steep.

## Getting Started
The first impression of emacs is quite harsh and uggly. Start with this light configuration instead:
```
curl http://mickael.kerjean.me/assets/files/emacs.conf > ~/.emacs
```

You're now ready to bootup emacs:
```
emacs
```

The first thing you should do from here is getting familiar with the keybinding. To do that don't go on some random tutorial available on the internet, go straight to emacs and open the tutorial embedded inside emacs itself!

To open the embedded interactive tutorial, hand over your keyboard and type this very intuitive keys:
```
C-h t
```

If you've completed it, well done, you're on your way to master emacs, go grab a beer and congratulate yourself.

If you're not to drunk after your beer, go take a look at:
- [org mode](http://orgmode.org/worg/): a much better replacement for evernote
- [the awesome list of emacs plugin](https://github.com/emacs-tw/awesome-emacs): Install anything you want doing: ``` M-x package-install ```
- start a new journey in emacs internals and enter lisp territory
