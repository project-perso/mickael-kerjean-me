---
layout: post
title: A guide to compile your stuff
image: /assets/thumbnails/tux.jpg
category: pro
---

Because not everyone know how to do it

<!--more-->

The linux package manager fit the needs 99% of the time but for some specific use case you might have to compile stuff by yourself. I heard terrible wrong things about compiling something by yourself like it is risky and will result in unproven version of your software. Well guess how those package you install on your distrubtion comes from? Yes somebody did the work already and unless you update the code by yourself, you won't use any 'unproven version' of your software but the real official one everyone else is using.

I wrote this guide as I needed it to refer some colleagues so here it is. In this post we'll compile the phpinterpreter version 5.5.38 from source code to be use with apache. What actually matter here is the method as it is the same for any other software.

# Requirements:

Before getting started, you need a few things:
-   git: to grab a copy of the code you want to install. 99.9% of the time you can use git but if your code isn't available on github, you will most likely use one of the following: svn or mercurial.
-   install the tooling to be able to compile stuff, most of the time it's automake, autoconf and make and the compiler itself (gcc or g++)

We'll assume you're on ubuntu, if not replace 'apt-get' by 'yum' on centos or 'pacman' or whatever package manager you're using. To install the dependencies:

```
sudo apt-get install git build-essential automake
```

# Step1: Get a copy of the source code

For php, it basically consist on searching on github for the official repository. For php, it's <https://github.com/php/php-src>

Once you're in there, click on the branch button and find which version you want to use. It can be displayed as a branch or simply tagged in the tag tab.

In our example, we're interested in version 5.5.38 of php. Unfortunately there's no tag available for this version but is available in the branch called 5.5.38.

This is how we get the source code we're interested in:

```bash
# setup your proxy you want to use if needed:
http_proxy=http://proxy.company.com:1000
https_proxy=http://proxy.company.com:1000
# get the source code
git clone https://github.com/php/php-src
git checkout 5.5.38
git pull origin 5.5.38
```

# Step2: Compile the code

We now have the entire source code of php in version 5.5.38 in our computer. Most of the time, compiling a tool end up typings those commands in the terminal:

```
./configure
make
make install
```

Before going that road, it's good practice to look at some install documentation as:
-   you might need to install a few more dependencies to actually compile your software correctly
-   you might want to add some options in your configure script to tailored the tool to your needs

# Example with PHP 5.5.38

Basically the only requirements to compile something by hand is reading the doc, reading errors if you face some and use common sense.

In the case of PHP, there's a INSTALL file, it wasn't made for not being read so take a few minutes to look at it, it will give you all the information you need to compile php. If you run into trouble while compiling without having read it, it's not the tools fault but yours.

In our case, the install documentation specify it might need a few more things:

> -   autoconf: 2.13+ (for PHP < 5.4.0), 2.59+ (for PHP >= 5.4.0)
> -   automake: 1.4+
> -   libtool: 1.4.x+ (except 1.4.2)
> -   re2c: Version 0.13.4 or newer
> -   flex: Version 2.5.4 (for PHP <= 5.2)
> -   bison: Version 1.28 (preferred), 1.35, or 1.75

If you have followed the requirements sections you should be able to check everything is fine on your environment and install any other missing dependencies.


Once you're done with that keep reading and it want you to download to PHP source directly from their website. Basically it looks like this (just read the doc and use common sense):

```
wget http://au1.php.net/get/php-5.5.38.tar.gz/from/this/mirror
gunzip php-5.5.38.tar.gz
tar -xf php-5.5.38.tar
```

As state in the documentation, see which option you want to enable in your build:

```
cd php-5.5.38
./configure --help | less
```

In our case we want to support apache, mysql and soap. According to the doc we will need to have this line: `--with-apxs2=/usr/local/apache2/bin/apxs` However in my installation they weren't anything there so I had to lookup for this apxs thing. After a second spent on google I went [here](https://httpd.apache.org/docs/2.4/programs/apxs.html). Good news is our version of apache was install with the mod<sub>so</sub> module so there's no need to recompile apache but still we have to install apxs directly with:

```
sudo apt-get install apache2-dev
whereis apxs # return /usr/bin/apxs
```

```
./configure --enable-soap --with-mysql --with-apxs2=/usr/bin/apxs --with-mysqli --with-curl --with-openssl
make
```

From this point, if something is breaking, read the error message. It's likely that you miss something so just do as it says and use google only if necessary.

After configuring and building php a message says, don't forget to run 'make test'. So let's try it before installing it on our system:

```
make test
```

If all the test passes, it will give you even more confidence that what your installing is solid. If everything is looking good:

```
sudo make install
cp php.ini-development /usr/local/lib/php.ini
# edit your php.ini file as needed. I also had to change the error_reporting from E_ALL to:
# ;;error_reporting = E_ALL
# error_reporting = E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT

# don't forget to turn off the other log related stuff
```

Done!

we now need to enable the php module we just installed. On ubuntu you should have all available modules under: '/etc/apache2/mods-available'. You should see there that our installation of PHP just created a php5.load file (look at the timestamp to be sure that's the good one). As state in the installation documentation, we'll have to update our configuration to configure apache correctly with this extension:

```
sudo cat > /etc/apache2/mods-available/php5.conf <<EOF
<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>
EOF
```

The last part is now about saying to apache to use this new version of php instead of the current one. Now we know that there's our new php5 mod available and the associate conf file is ready so let's use it:

```
sudo a2dismod php*
sudo a2enmod php5
```
