---
layout: post
category: pro
image: /assets/thumbnails/docker.jpg
---

Manage the lifecycle or your containers and images using docker

<!--more-->

# Manage Containers
- start all your idle containers:
```docker ps -a | awk '{print $1}' | xargs docker start```
- stop all running containers:
```docker ps | awk '{print $1}' | xargs docker stop```
- create a container:
````
REF_CONTAINER=foo
REF_IMAGE=foo
PORT_HOST=8082
PORT_CONTAINER=80
docker run --detach --name $REF_CONTAINER -p $PORT_HOST:$PORT_CONTAINER $REF_IMAGE
```
- enter in a container:
```
REF_CONTAINER=foo; # Can be an id or a name
docker exec -ti $REF_CONTAINER bash
```
- run a command inside a container:
```
REF_CONTAINER=foo; # Can be an id or a name
COMMAND=whoami
docker exec -ti $REF_CONTAINER $COMMAND
```

# Manage images
- create/update an image:
```
IMAGE_NAME=foo
docker build . --name $IMAGE_NAME
````
- clean images:
```
# remove all images that contains the "" tag
docker images | grep "" | awk '{print $3}' | xargs docker rmi
```

# A note on the firewall configuration
Using iptables (memo: /etc/sysconfig/iptable):
```
NET_INTERF=enp0s25

# RESET FIREWALL
iptables -F
# local loop
iptables -A INPUT -i lo -j ACCEPT
# BASIC APPLICATION
iptables -A INPUT -p tcp --dport 22 -j ACCEPT # SSH
# ...
# add more application if needed here

# DOCKER
iptables -A FORWARD -i $NET_INTERF -o docker0 -j ACCEPT # DOCKER BRIDGE: COMING REQUEST
iptables -A FORWARD -i docker0 -o $NET_INTERF -j ACCEPT # DOCKER BRIDGE: LEAVING REQUEST
# communication between host and container
iptables -A INPUT -i docker0 -j ACCEPT
# DROP EVERYTHING ELSE
iptables -P INPUT DROP
iptables-save | sudo tee /etc/sysconfig/iptables
service iptables restart

# START APPLICATIONS
service docker restart
docker ps -a | awk '{print $1}' | xargs docker start # start containers
```

New configuration can be shown: ``` iptables -L -n ```
