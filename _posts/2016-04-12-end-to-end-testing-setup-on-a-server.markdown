---
layout: post
category: pro
image: /assets/thumbnails/lab.jpg
---

Let's setup a proper environment allowing testing against real browser in a real server without graphical interface
<!--more-->

# INSTALL
```
cd && mkdir selenium && cd selenium;
wget http://selenium-release.storage.googleapis.com/2.44/selenium-server-standalone-2.44.0.jar
apt-get install openjdk-7-jre-headless -y
apt-get install firefox -y
apt-get install -y xvfb xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic
```

# USAGE
```
DISPLAY=:1 xvfb-run java -jar ~/selenium/selenium-server-standalone-2.44.0.jar
```
we can now start any graphical application in a non graphical environment as xcfb emulate a X11 environment!
