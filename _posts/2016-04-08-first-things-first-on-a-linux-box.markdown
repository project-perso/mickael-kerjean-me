---
layout: post
category: pro
tags: snippet
image: /assets/thumbnails/tux.jpg
---

Up and running in a sec

<!--more-->

```
# variables
USER=foo
# script
adduser $USER
usermod -aG sudo $USER
# For centos: visudo and append to the file $USER ALL=(ALL) ALL
sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
apt-get update
apt-get install fail2ban sudo curl tmux htop emacs git tig tree nginx unzip apache2-utils
service sshd restart
```

```
# postgres 9.5
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib postgresql-9.5-postgis-2.2
```
