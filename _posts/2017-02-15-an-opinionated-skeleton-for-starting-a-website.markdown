---
layout: post
category: pro
image: /assets/thumbnails/html5.jpg
---

A HTML/CSS skeleton to quickly get started with your project

<!--more-->

Getting started with a new website or application always involve doing some sort of copy and paste to generate some skeletons.
Here is the html and css I'm starting with when creating a new project.

# HTML

## Skeleton
{% highlight html %}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="name" name="apple-mobile-web-app-title">
    <meta content="black-translucent" name="apple-mobile-web-app-status-bar-style">

    <title></title>
    <meta name="description" content="">
    <link rel="prefetch" href="/">
    <script src="/assets/js/app.js"></script>
    <link rel="stylesheet" href="/assets/css/style.css">
    <!--[if lte IE 9]>
        <script src="https://rawgit.com/jonathantneal/flexibility/master/flexibility.js"></script>
        <script> flexibility(document.body)</script>
    <![endif]-->
  </head>

  <body>

  </body>
</html>
{% endhighlight %}


## Explanations

- ```<meta http-equiv="X-UA-Compatible" content="IE=edge">```: Yes we should force IE to use edge unless you want to spend countless hours supporting IE7. Yes it happens! If you wonder why, in my company the IT department decided to migrate to IE11 once IE8 support was drop and to avoid issues with legacy application, they updated everyone registry to use IE7 rendering engine instead of the default edge. Just in case, it's worth nothing but put this line in your skeleton
-  ```<meta content="....." name="...">```: that's what will make your website looks ok on mobile. Some people will argue about ```maximum-scale=1, user-scalable=no``` because of some accessibility issues it might cause. It's true but let's face the reality, if you don't have it, a focus on an input will make your website flacky and accessibility on the web is poorly done by browser vendor anyway. It's actually so bad that even google in Australia is using the maximum-scale trick, github, yahoo and many many others.
- ```<title></title> <meta name="description" content="">```: the basic tags that are here for SEO
- ```<link rel="prefetch" href="/">```: the goal here is to preload your homepage or any page you think will be click after this page to improve the end user experience.
- ```<script src="/assets/js/app.js"></script><link rel="stylesheet" href="/assets/css/style.css">```: load your stylesheet and script
- ```
    <!--[if lte IE 9]>
        <script src="https://rawgit.com/jonathantneal/flexibility/master/flexibility.js"></script>
        <script> flexibility(document.body)</script>
    <![endif]-->
```: Creating layout in flexbox is a step forward and supporting IE shouldn't be a reason to not using it. This adds flexbox support on the boring IE


# CSS

## Skeleton
{% highlight css %}
html{
    touch-action: manipulation;
    -webkit-text-size-adjust:100%;
    font-family:"San Francisco","Roboto","Arial",sans-serif;
    *{box-sizing: border-box;}
}
body{margin: 0;}
{% endhighlight %}

## Explanations
- ```touch-action: manipulation;```: Did you already experience the 300ms tap delay on mobile? It's quite boring and this css directive fixes it. More details [here](https://developers.google.com/web/updates/2013/12/300ms-tap-delay-gone-away)
- ```-webkit-text-size-adjust: touch```: Did you already try to look at a website in landscape mode on a iphone? For no reason, the font-size becomes bigger. Obviously you don't want that and this directive will make your font remain the same wether you're in portrait or landscape mode.
- ```font-family:"San Francisco","Roboto","Arial",sans-serif```: Usually the default platform font are quite good and your users are already familiar with them, why would you want to change that? San francisco is the default font for [every apple device](https://developer.apple.com/fonts/) whereas Roboto is the one to use for Android devices and for the boring windows Arial is the go to font. A fallback to sans-serif is always usefull for edge case where your user is neither on those 3 OS.
- ```*{box-sizing: border-box;}``` If you don't want to make calculation of the with and height that involve the border size in your layout, use this, it will make your life much easier.
- ```body{margin: 0;}```: Get rid of this irritating margin in the body that is always part of the default stylesheet regardless the browser you're using.
