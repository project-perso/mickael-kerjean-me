---
layout: post
category: pro
tags: [gitlab, mattermost]
image: /assets/thumbnails/gitlab.jpg
---

Maintenance process for gitlab/mattermost

<!--more-->

# Location for the backups
``` /backups ```

# Backing up

Just copy and paste this entire script

```
cat << EOF | sudo bash
#!/bin/bash
function backup_gitlab {
  rm -f /var/opt/gitlab/backups/*
  gitlab-rake gitlab:backup:create
  mv /var/opt/gitlab/backups/* /backups
}
function backup_mattermost {
  # init
  rm -rf /tmp/mattermost || true && \
  mkdir -p /tmp/mattermost/data
  # backup data
  cp -R /var/opt/gitlab/mattermost/* /tmp/mattermost/data && \
  chmod 777 /tmp/mattermost
  # backup db
  cd /tmp/mattermost && \
  /opt/gitlab/embedded/bin/pg_dump -U gitlab_mattermost -h /var/opt/gitlab/postgresql -p 5432 mattermost_production > mattermost_production_backup.sql
  # package and cleanup
  cd /tmp && \
  tar -zcf mattermost.tar.gz mattermost && \
  rm -rf mattermost && \
  mv mattermost.tar.gz /backups
}

export -f backup_mattermost
export -f backup_gitlab

#######
## MAIN
function main {
  # initialize
  sudo rm -rf /backups && \
  sudo mkdir /backups > /dev/null 2&>1 || true && \
  chmod -R 777 /backups
  
  # main
  su root -c "bash -c backup_gitlab"
  su mattermost -c "bash -c backup_mattermost"

  # finalize
  sudo chmod 770 /backups
}
main
EOF
```


# Restore

## Gitlab
```
su -
mv /backups/*gitlab_backup.tar /var/opt/gitlab/backups/
chown git:root /var/opt/gitlab/backups/*
BACKUP=`ls /var/opt/gitlab/backups/ | grep gitlab_backup | head -n 1 | sed -e 's/_gitlab_backup\.tar$//p' | head -n 1`
# Restore as on https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/raketasks/backup_restore.md#user-content-restore-a-previously-created-backup
# Stop processes that are connected to the database
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq
gitlab-rake gitlab:backup:restore

# start gitlab again
gitlab-ctl start
```

## Mattermost
```
# mattermost
su -
tar -zxvf /backups/mattermost.tar.gz -C /backups/
chmod -R 777 /backups
mv /backups/mattermost/data/* /var/opt/gitlab/mattermost/
su mattermost
/opt/gitlab/embedded/bin/psql -U gitlab_mattermost -h /var/opt/gitlab/postgresql -p 5432 mattermost_production < /backups/mattermost/mattermost_production_backup.sql
rm -rf /backup/mattermost
```
