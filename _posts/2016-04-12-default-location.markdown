---
layout: post
category: pro
image: /assets/thumbnails/tux.jpg
---

Let's make things easier to find on ubuntu

<!--more-->

# Config: Web related application
- PHP.ini: ```/etc/php.ini```
- Apache: ```/etc/apache2/site-available/default``` ```/etc/httpd/conf.d/```
- nginx: ```/etc/nginx/site-enable/default```

# Config: System related application
- python easy_install packages: ```/usr/lib/python2.7/site-packages/```
