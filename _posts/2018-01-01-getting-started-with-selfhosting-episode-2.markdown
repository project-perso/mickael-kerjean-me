---
layout: post
title: Getting started with self hosting - episode 2
date: 2017-12-26
image: /assets/thumbnails/server.png
category: pro
---

Creating your own cloud, look at the infrastructure

<!--more-->

Without a server, you won't do much. At the end of this post, you will have a server ready to rock

# Server

It might sounds obvious but to self host your own set of applications, you need a server. That's probably the first choice you'll have to make throughout your journey:

> where do you host the server(s)?

You have 2 possibilities:

1.  **Host it at home**: the only advantage is you have a direct access to your machine. A few things about this:
    -   real servers make real noises. Even if you think it's cool because it feels pro, you don't want this at home if you're not living alone on an island, it can be very noisy. My advice is to keep it cheap and simple by using whichever hardware you already have (or buying a [raspberry pie](https://www.raspberrypi.org/)), you'll know when you need more, until then, relax and don't invest much.
    -   don't think it's free, you'll still be paying for electricity and that won't necessarily be cheaper than renting a machine in the cloud
    -   you'll have to setup a bunch of things:
        -   setup your router so that HTTP/HTTPS traffic coming from the internet will be forward to your server (that's what we call [port forwarding](https://en.wikipedia.org/wiki/Port_forwarding))
        -   request a fixed IP to your internet provider. If that's not possible, you can look at [dyndns](http://dyn.com/dns/) but keep in mind those solutions are flaky by design.
    -   pay attention to your internet connection. Outside LAN, if your connection isn't great, you might end up with slow unresponsive applications. If you care about this, I would advice to go with the rental option
2.  **Rent it from a hosting company**: the choice will be between a VPS (which stands for virtual private server, aka a virtual machine) or a dedicated server with various support level agreement. If you're hesitating between both, the rule of thumb is to start with a small VPS and get more when you need more. There's no point in having a large machine if it stays idle all the time.

**Tips:** Weigh the pro and cons and make a decision.

**Tips:** If you go with option 2 I would recommend [Digital Ocean](https://link.kerjean.me/digital-ocean). Everything has been so far cheap, nice and easy. On the plus side, you can also have 2 month free credit with their cheapest machine if you create an account with this [link](https://link.kerjean.me/digital-ocean). I've also used [Vultr](https://link.kerjean.me/vultr) and [EC2](https://aws.amazon.com/ec2/) without problems. If you consider using OVH, for your own sake, run away. I've been using them for 5 years without an inch until recently where:
-   I needed to use their support which is unresponsive, takes forever to get something done and automatically close tickets even if the issue isn't solve
-   they refused to provide me with an invoice in English as they've already issued one in French
-   they decided to sue me on the basis I needed to pay another year of a service I didn't want to renew as I didn't turn off their yearly automatic renewal in time.

# OS

Choosing an OS is like choosing between white/dark and milk chocolate. There's no absolute truth, it's only a matter of taste. Anything could do the job but Linux will be our pick to follow this guide. Like for chocolate, your favorite milk chocolate could be Lindt or Ferrero, in the Linux world, this is what we called "distribution". In this guide, the instructions will be given for the Linux distribution called [Ubuntu server](https://www.ubuntu.com/download/server).

**Tips:** Pick any Linux distribution you're already familiar with. There's no absolute obligation for this but try to pick a server distribution over a desktop one as there are a few differences that makes them better for the task.

# Establish a connection to your server

Being able to connect to your server is an absolute requirement. The industry standard to connect to a server is [SSH](https://en.wikipedia.org/wiki/Secure_Shell) and it will be our preferred way to do things. You might hear about some other way to connect to your server like [VNC](https://en.wikipedia.org/wiki/Virtual_Network_Computing), [RDP](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol) or [cPanel](https://cpanel.com/) but we will simply ignore those as they are second class citizen when it comes to server administration. SSH will make your life a bit harder at the beginning if you're not already familiar with it but it's a mid/long term win compared to the other solution that will make your life easier only at the very beginning and make you head bang on the wall as soon as you're trying to achieve something they don't have a GUI for.

To use SSH, you will need 2 pieces of software: one installed in the server and another one installed on your laptop or desktop machine (called the client). If you've chosen to go with:
-   a cloud provider: the SSH server will come already pre-installed. Basically you just have to wait until you receive the credentials to your newly created machine. Those credentials will be the one you can use to initiate a SSH connection to your server.
-   home hosting, plug a screen and a keyboard in your server and process with the installation of the operating system of your choosing. Once complete, you will need to install the so called SSH server by typing:

    ```bash
    # instruction for a debian based linux distribution
    sudo apt-get update
    sudo apt-get install openssh-server
    ```

To connect to your server, you will need a software installed in your laptop called a `SSH client`. If you are:
-   a Linux / OSX user: it is very likely the SSH client is already pre installed. fire up a terminal and type:

    ```bash
    ssh username@host_ip
    # replace username by your actual username, it might be 'root' by default
    # replace host_ip by the ip adress of your machine
    ```
-   a windows user: give a try to [putty](http://www.putty.org/), enter the ip address of your machine and connect

If everything is working fine, you should be welcome with a message and a prompt you can type commands from. For example:

```bash
whoami
date
```

# Get yourself a domain name

Imagine a world where you'd access google by typing in your browser: <http://216.58.203.110/>. That would be quite a rough experience. Essentially a domain name is a convenient way for your users to access a service. The entire purpose of DNS is to translate a "friendly name" to an ip address.

It's not a must have but it's definitely recommended to purchase one. It cost a few bucks per year and the most popular provider is [GoDaddy](https://link.kerjean.me/godaddy-domain) I've used them a lot in the past and never got any problem. If you get a domain with [this link](https://link.kerjean.me/godaddy-domain) I will earn a few bucks which is a great way to show support if you like this post and want more in the future.

In the very likely scenario where you have 1 machine and 1 IP address, I would recommend to do the following once you've purchased a domain name:
1.  Create an A record: `yourdomain.com => ip address`
2.  Create a CNAME record: `www.yourdomain.com => yourdomain.com`
3.  Everytime you want to create a new service, you will need to generate a new CNAME record: `application_name.yourdomain.com => yourdomain.com`

**Tips**: If you really don't want to invest in a domain name, you have 2 options to access your services:
-   you'll have to type in your browser the ip address of your server followed by the port the application is running (eg: <http:/24.34.12.234:1010>). It won't be user friendly, slower (no gzip compression), less secure (no web application firewall) and good luck to explain this to a non tech savvy guy
-   you can trick your OS dns resolution by editing your host file (`/etc/hosts` in linux and OSX). Example: if you add: "`192.168.0.17 selfhosted.com`" in your host file, typing selfhosted.com in your browser will bring you to 192.168.0.17. It will be better than the approach above but anybody who needs to access your applications will have to update their host file as well. As a side note, this tip can also be applied to block advertising in your browser without any third party plugin :).

# What now?

By default your server isn't really setup or even secure. The [next episode]({% post_url 2018-01-01-getting-started-with-selfhosting-episode-3 %}) will focus on how to setup your server
