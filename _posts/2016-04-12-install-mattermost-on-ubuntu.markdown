---
layout: post
category: pro
image: /assets/thumbnails/mattermost.jpg
---

Step by step guide to install an open source alternative to Slack
<!--more-->

# depedencies
``` bash
# install depedencies
su -
apt-get install -y mysql-server
useradd -r -s /bin/false mattermost
mkdir /data
mkdir /data/apps
cd /data/apps
# pull code
wget https://releases.mattermost.com/2.1.0/mattermost-team-2.1.0-linux-amd64.tar.gz
tar -xzvf mattermost*.tar.gz && rm -rf mattermost*.tar.gz
cd mattermost
```

# configure application
``` bash
# setup database
# mysql -u root -e "DROP DATABASE IF EXISTS mattermost; DROP USER 'madmin'@'localhost'; FLUSH PRIVILEGES;"
mysql -u root -e "CREATE DATABASE mattermost; CREATE USER 'madmin'@'localhost' IDENTIFIED BY 'madmin'; GRANT ALL PRIVILEGES ON mattermost.* TO 'madmin'@'localhost';"
sed -i 's/DataSource\".*/DataSource\": \"madmin:madmin@tcp(127.0.0.1:3306)\/mattermost\?charset=utf8mb4,utf8\",/' ./config/config.json

# setup emails notification
sed -i 's/SendEmailNotifications\":.*/SendEmailNotifications\": true,/' /data/apps/mattermost/config/config.json
sed -i 's/SMTPUsername\":.*/SMTPUsername\": \"spotlive\.io@gmail\.com\",/' /data/apps/mattermost/config/config.json
sed -i 's/SMTPPassword\":.*/SMTPPassword\": \"spotlive\",/' /data/apps/mattermost/config/config.json
sed -i 's/SMTPServer\":.*/SMTPServer\": \"smtp\.gmail\.com\",/' /data/apps/mattermost/config/config.json
sed -i 's/SMTPPort\":.*/SMTPPort\": \"587\",/' /data/apps/mattermost/config/config.json
sed -i 's/ConnectionSecurity\":.*/ConnectionSecurity\": \"STARTTLS\",/' /data/apps/mattermost/config/config.json
# other settings
sed -i 's/EnableIncomingWebhooks\":.*/EnableIncomingWebhooks\": true,/' /data/apps/mattermost/config/config.json
sed -i 's/EnableOutgoingWebhooks\":.*/EnableOutgoingWebhooks\": true,/' /data/apps/mattermost/config/config.json
sed -i 's/EnableCommands\":.*/EnableCommands\": true,/' /data/apps/mattermost/config/config.json
sed -i 's/EnablePostUsernameOverride\":.*/EnablePostUsernameOverride\": true,/' /data/apps/mattermost/config/config.json
sed -i 's/EnablePostIconOverride\":.*/EnablePostIconOverride\": true,/' /data/apps/mattermost/config/config.json
# disable team creation and user registration
sed -i 's/EnableUserCreation\":.*/EnableUserCreation\": false,/' /data/apps/mattermost/config/config.json
sed -i 's/EnableTeamCreation\":.*/EnableTeamCreation\": false,/' /data/apps/mattermost/config/config.json
sed -i 's/EnableTeamListing\":.*/EnableTeamListing\": true/' /data/apps/mattermost/config/config.json
# update default bot icon
wget https://2a20b3436652badecec7f5565169499ed7611b19.googledrive.com/host/0BysbKfsqFqzKV1hkZ0h5b1FNWXc/baymax.png
mv baymax.png /data/apps/mattermost/web/static/images/webhook_icon.jpg
# setup backup
pip install gsync
# setup launcher: supervisor
# cd ./bin/ && ./platform
```

# Create a service
- put the following under:  /etc/init.d/mattermost

```

#! /bin/sh
### BEGIN INIT INFO
# Provides:          mattermost
# Required-Start:    $network $syslog
# Required-Stop:     $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Mattermost Group Chat
# Description:       Mattermost: An open-source Slack
### END INIT INFO

PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="Mattermost"
NAME=mattermost
MATTERMOST_ROOT=/opt/mattermost
MATTERMOST_GROUP=mattermost
MATTERMOST_USER=mattermost
DAEMON="/bin/platform"
PIDFILE=/var/run/.pid
SCRIPTNAME=/etc/init.d/

. /lib/lsb/init-functions

do_start() {
    # Return
    #   0 if daemon has been started
    #   1 if daemon was already running
    #   2 if daemon could not be started
    start-stop-daemon --start --quiet         --chuid : --chdir  --background         --pidfile  --exec  --test > /dev/null         || return 1
    start-stop-daemon --start --quiet         --chuid : --chdir  --background         --make-pidfile --pidfile  --exec          || return 2
}

#
# Function that stops the daemon/service
#
do_stop() {
    # Return
    #   0 if daemon has been stopped
    #   1 if daemon was already stopped
    #   2 if daemon could not be stopped
    #   other if a failure occurred
    start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5         --pidfile  --exec
    RETVAL="130"
    [ "" = 2 ] && return 2
    # Wait for children to finish too if this is a daemon that forks
    # and if the daemon is only ever run from this initscript.
    # If the above conditions are not satisfied then add some other code
    # that waits for the process to drop all resources that could be
    # needed by services started subsequently.  A last resort is to
    # sleep for some time.
    start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5         --exec
    [ "130" = 2 ] && return 2
    # Many daemons don't delete their pidfiles when they exit.
    rm -f
    return ""
}

case "" in
start)
        [ "" != no ] && log_daemon_msg "Starting " ""
        do_start
        case "130" in
                0|1) [ "" != no ] && log_end_msg 0 ;;
                2) [ "" != no ] && log_end_msg 1 ;;
        esac
        ;;
stop)
        [ "" != no ] && log_daemon_msg "Stopping " ""
        do_stop
        case "130" in
                0|1) [ "" != no ] && log_end_msg 0 ;;
                2) [ "" != no ] && log_end_msg 1 ;;
        esac
        ;;
status)
    status_of_proc "" "" && exit 0 || exit 130
    ;;
restart|force-reload)
        #
        # If the "reload" option is implemented then remove the
        # 'force-reload' alias
        #
        log_daemon_msg "Restarting " ""
        do_stop
        case "130" in
        0|1)
                do_start
                case "130" in
                        0) log_end_msg 0 ;;
                        1) log_end_msg 1 ;; # Old process is still running
                        *) log_end_msg 1 ;; # Failed to start
                esac
                ;;
        *)
                # Failed to stop
                log_end_msg 1
                ;;
        esac
        ;;
*)
        echo "Usage:  {start|stop|status|restart|force-reload}" >&2
        exit 3
        ;;
esac

exit 0

```


- Complete the setup
```
chmod +x /etc/init.d/mattermost
systemctl enable mattermost.service
chown -R mattermost:mattermost /data/apps/mattermost/
service mattermost start
```

# Tips
## Theme
Account settings -> display -> theme
```
#2b333d,#ffffff,#333c47,#c5d0de,#c5d0de,#24272d,#24272d,#c5d0de,#c5d0de,#E0B333,#24272d,#ffffff,#faf9f9,#3d3f44,#F80,#fff2bb,#3084bb,#3084bb,#26a970,#ffffff,github
```

## CLI to send message
```
# https://github.com/mtorromeo/mattersend
pip install pathlib configparser mattersend
```

## Matterbot, our nice little bot
[documentation](http://mattermost-bot.readthedocs.org/deploy.html)
```
pip install mattermost_bot
```
plugins leave under /usr/local/lib/python2.7/dist-packages/mattermost_bot/plugins.
```
