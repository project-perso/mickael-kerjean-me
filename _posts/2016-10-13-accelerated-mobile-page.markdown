---
layout: post
published: false
---

Make you page load instantly

# The AMP project
That's one of the things Google came out with as a solution to speed up websites improving drastically the first time to display.

Basically, it's just a set of tools that allow your website to load faster by doing all sort of optimization.

Sounds cool but it's not a one framework to rule them all solutions as it comes with a major drawback:
You can't use any of your javascript.

# How it works
If you want to create an AMP pages, you simply need to create your pages using simply html and css. AMP gives you a few constraints on how to write it and you'll need to import a few things to load the validator.

It's not because you can't use js that you can't have any interactivity. The project defines a few components that you can use like amp-user-notification, lightbox, social media integration. Many examples of those components can be found [here](https://ampbyexample.com/) and the entire documention is [here](https://www.ampproject.org/docs/reference/components). However the quality of those components is [variable](https://ampbyexample.com/components/amp-image-lightbox/), AMP definitly can't fit everyone's need.

Validate your AMP page is as easy as appending #development=1 at the end of your URL. Once it's valid, you can access it throught the google AMP cache which makes your page even faster and can be use free of charge!

For example, this post is AMP compatible and you can access it from the [google AMP cache]().

# A AMP theme for Jekyll
The entire source code for this website has been made available on Gitlab.
Want to give it a try? Just fork it and your pages will become available on Gitlab pages at the following url: ```${GITLAB_USERNAME}.gitlab.io/${PROJECT_NAME}```

By the way, this Jekyll theme come with a few more features than simply AMP:
- a search bar
- comments integration using disqus
- rocking fast on mobile
- support for microdata, json+ld and open graph
- full fledge gallery for flickr


