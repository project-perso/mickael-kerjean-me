---
layout: post
title: Getting started with selfhosting - episode 1
date: 2017-12-26
image: /assets/thumbnails/server.png
category: pro
redirect_from: /2017/07/16/getting-started-with-selfhosting/
---

Beginning of our journey to self host applications on your own cloud

<!--more-->

Whether you don't like seeing your data sold to the highest bidder, feel concerned about data privacy, worry about what's gonna happen when the next Saas company you're relying on will get bought, disappear or hacked, there's many reasons for wanting to self host application.

The fact is, there's a lot of very decent alternative to most of the application you're already using. I'm writing this guide hoping it will help you getting started by sharing my experience considering:
-   I happen to run my own self hosted environment and the entire stack is running nicely.
-   They aren't so many guides available and some of those guides do it wrong ([this one for example](https://www.youtube.com/watch?v=L8AW6U4GIPs) gives you wrong information about SSL)
-   Those guides usually don't tell you about the trade off involved in selecting a piece of tech. Disadvantages aren't proudly market anywhere and you might learn about them the hard way.

Our moto for this guide is to help you making the right decisions for your installation. Most of the time, it's a tradeoff you have to choose from. Keep in mind, there's always a cost to something that look like a great idea and I'll try to expose those at the best of my knowledge.

# General Approach to self hosting

Depending on your enthusiasm, experience and level, you might consider two different path:
1.  install an prepackage solution for self hosting such as:
    -   [yunohost](https://yunohost.org/)
    -   [cloudron](https://cloudron.io/)
    -   [sandstorm](https://sandstorm.io/)
    -   [Cozy](https://cozy.io)
2.  go with a fully custom solution that you build from scratch

If you're not willing to learn about Linux and server administration, you don't have a choice `=> option 1`. Option 1 gives you ease of installation at the cost of being limited in term of which application you can install and how it is setup. To make it happen, go to the getting started page of any of the website listed above and you should be ready to go quickly.

Option 2 is what this serie is all about. It's definitely harder but I'll be with you along the way so that you can install and customize pretty much everything you need with the only limit being your imagination (and not a limited app store).

# Program for this tutorial

This is the program we will follow in this tutorial series:
-   [episode 2]({% post_url 2018-01-01-getting-started-with-selfhosting-episode-2 %}): Creating your own cloud starting from the infrastructure
-   [episode 3]({% post_url 2018-01-01-getting-started-with-selfhosting-episode-3 %}): Correctly setup your server
-   [episode 4]({% post_url 2018-01-01-getting-started-with-selfhosting-episode-4 %}): Setup an application
-   episode 5: Maintaining your server
-   episode 6: Making things better
-   episode 7: Making things easier

To get started, jump straight to the [second episode]({% post_url 2018-01-01-getting-started-with-selfhosting-episode-2 %}).
