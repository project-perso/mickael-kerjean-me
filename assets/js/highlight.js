window.onload = function(){
    // code highlight
    var $els = document.querySelectorAll('.highlight');
    for(var i=0; i< $els.length; i++){    
	hljs.highlightBlock($els[i]);
    }

    // setup autofocus for desktop only
    $els = document.querySelector('input[data-focus]');
    if( ('ontouchstart' in window) === false && $els){
	$els.focus();
    }
}


